#include "funcionario.hpp"
#include <iomanip>
#include <iostream>

using namespace std;

int main(int argc, char ** argv){

    Funcionario funcionario1;

    funcionario1.set_nome("Douglas");
    funcionario1.set_cpf(234234244234);
    funcionario1.set_telefone("77678-3455");
    funcionario1.set_email("douglas@gmail.com");
    funcionario1.set_salario(1500.0f);
    funcionario1.set_funcao("Caixa");

    cout << "Nome: " << funcionario1.get_nome() << endl;
    cout << "CPF: " << funcionario1.get_cpf() << endl;
    cout << "Telefone: " << funcionario1.get_telefone() << endl;
    cout << "Email: " << funcionario1.get_email() << endl;
    cout << "Salário: " << fixed << setprecision(2) << funcionario1.get_salario() << endl;
    cout << "Função: " << funcionario1.get_funcao() << endl;

    return 0;
}




